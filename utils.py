import pymongo
import requests
import numpy as np
import cv2
import os

client = pymongo.MongoClient("mongodb://localhost:27017/")
database = client["us-terrain"]


def terrainsaver(geopos, heightdata):
    print("Saving to mongodb")
    collection = database[geopos]
    for cellrow in range(12):
        for cellcol in range(12):
            print("[" + str(cellrow).zfill(2) + ", " + str(cellcol).zfill(2) + "] WORKING")
            cell = {
                "z": cellrow,
                "x": cellcol,
                "terrain": []
            }
            for z in range(901):
                row = heightdata[(901 * cellrow) + z][cellcol:cellcol+901]
                cell["terrain"].append(row)
            print("[" + str(cellrow).zfill(2) + ", " + str(cellcol).zfill(2) + "] SAVING")
            # print(cell["terrain"])
            result = collection.insert_one(cell)
            print("INSERTED WITH ID: " + str(result.inserted_id))
            print("[" + str(cellrow).zfill(2) + ", " + str(cellcol).zfill(2) + "] DONE")


def translateposition(lat, lon):
    pos = ""
    if lat > 0:
        pos += "n" + str(lat).zfill(2)
    elif lat < 0:
        pos += "s" + str(-lat).zfill(2)
    if lon > 0:
        pos += "e" + str(lon).zfill(3)
    elif lon < 0:
        pos += "w" + str(-lon).zfill(3)
    return pos


def imageread(position):
    url = "https://prd-tnm.s3.amazonaws.com/StagedProducts/Elevation/13/TIFF/" + position + "/USGS_13_" + position + ".tif"
    print("DOWNLOADING IMAGE")
    resp = requests.get(url)
    print("SAVING IMAGE")
    open("temp/"+position+"_cache.tif", "wb").write(resp.content)
    print("READING DATA")
    image = cv2.imread("temp/"+position+"_cache.tif", cv2.IMREAD_ANYDEPTH)
    os.remove("temp/"+position+"_cache.tif")
    print("DOWNLOADING DONE")
    # return the image
    return image
