from utils import terrainsaver, translateposition, imageread
import json

lat = 49
lon = -115

position = translateposition(lat, lon)

print(position)

image = imageread(position)

print(image)

arr2d = image.tolist()

result = terrainsaver(position, arr2d)
